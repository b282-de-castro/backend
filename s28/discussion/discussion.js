// CRUD Operations

//Create Operation
// insertOne() - inserts one document to the collection/table
db.users.insertOne({
	"firstName":"John",
	"lastName":"Smith"
});

// insertMany() - Inserts multiple documents to the collection

db.users.insertMany([
	{"firstName":"John", "lastName":"Doe"},
	{"firstName":"Jane", "lastName":"Doe"}
]);

// Read Operation
// find() - get all the inserted users
db.users.find();

// Retrieving specific documents
db.users.find({"lastName":"Doe"});

//Update Operation
//updateOne() - modify one document
db.users.updateOne(
{
	"_id": ObjectId("648af8ec1e8b13db29bc0893")
},
{
	$set: {
		"email":"johnsmith@gmail.com"
	}
}
);

//updateMany() -  modify multiple docments
db.users.updateMany(
{
	"lastName":"Doe"
},
{
	$set{
		"isAdmin":false
	}
}
);

// Delete Operations 
//deleteMany - deletes multiple documents
db.users.deleteMany({"lastName":"Doe"});

//deleteOne - deletes single document
db.users.deleteOne({"_id": ObjectId("648af8ec1e8b13db29bc0893")});