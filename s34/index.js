const express = require ("express");

const app = express();

const port = 3000;

app.use(express.json());
app.use(express.urlencoded({extended: true}));

//[SECTION] Routes
// GET


app.get("/greet", (request, response) => {
	response.send("Hello from the /greet endpoint!")
});

// POST
app.post("/hello", (request, response) => {
	response.send(`Hello there, ${request.body.firstName} ${request.body.lastName}`);
});

// Simple registration

let users = [];

app.post("/signup", (request, response) =>{
	if (request.body.username !== "" && request.body.password !== ""){
		users.push(request.body);

		response.send(`User ${request.body.username} successfully registered!`);
	} else {
		response.send("Please input BOTH username and password!")
	}

});


/*
{
	"username":"makicutie",
	"password": "Imprettychihuahua"
}
*/

//[SECTION] Activity

app.get("/home", (request, response) => {
	response.send("Welcome to the home page")

});

app.get("/users", (request, response) => {
	response.json(users)

});

app.delete("/deleteUser", (request, response) => {
    let username = request.body.username;
    let deleted = false;

    for(let i = 0; i < users.length; i++){
        if(users[i].username == username){
            users.splice(i, 1);
            deleted = true;
            break;
        }
    }
    
    if(deleted){
        response.send(`User ${username} has been deleted.`);
    } else {
        response.send(`User ${username} not found.`);
    }
});




app.listen(port, () => console.log (`Server running at port ${port}`));

