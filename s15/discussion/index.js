//console.log ("Hello World!")

/* undefined
null - nothing
boolean - true or false
symbo - unique value
number
object - store key value pairs 

store data in a variable
a variable is like a box you can fill in with anything
to dclare variable*/

//var myName = "Belle"

//myName = 8


/* 3 ways to declare a variable
 1 var - used throughout the whole program
 2 let - will only be used within the scope of where you declared that 
 3 const - is a variable that should never change. you can never change it. it is constant*/

//let ourName = "zuitstudent"

/* Storing values with assignment operator - 9.16 */


//var a; 
/* declaring the variable - end all lines in javascript with semicolon */


//var b = 2; 
/* declaring and assigning variable in one line. This means we are assigning 2 to b*/
//console.log(a)
//a = 7;

//b = a;

//console.log(a)

/* initializing Variables w/ assignment operator*/

//var a = 9;


/* uninitialized variables */

//Initialized these three variables

//var a = 5;
//var b = 10;
//var c = "I am a";


//Do not change the code below this line

//a = a + 1;
//b = b + 5;
//c = c + "String!";

/* Case Sensitivity in Variables

/*variable names and function names in Javascript is case sensitive*/


//Declarations

/*var studlyCapVar;
var properCamelCase;
var titleCaseOver;*/


//Assignments

/*studlyCapVar = 10;
properCamelCase = "A String"
titleCaseOver = 9000;

console.log (studlyCapVar)*/

//Adding Numbers


/*var sum = 10 + 10;
console.log (sum)*/

//subtracting numbers

/*var difference = 45 - 15;
console.log (difference)*/

//multiplying numbers

/*var product = 8 * 10;

console.log (product)*/

//Dividing Numbers

/*var quotient = 66 / 10;

console.log (quotient)*/

//Incrementing numbers

/* to increment a number means to add 1 to it*/

//var myVar = 87;

//Only change code below this line

//myVar = myVar + 1;

//or do

/*myVar++;

console.log (myVar)*/

// Decrementing Numbers

/*var myVar = 10;

myVar = myVar - 1;

myVar--;*/

/*Decimal Numbers - refers to flaoting point number or floats. Anything that has decimal point init is a decimal point number*/

//var ourDecimal = 5.7;

//multiply decimals - works the same as multiplying an integer

/*var muldecimal = 2.5 * 2.5 ;*/

//divide decimal

//var quotient = 4.4 / 2.0;

/*Finding a remainder - remainder operator looks like a %. Its often used to find if the number is even or odd. even when divisible by 2 with 0 as remainder

var remainder;
remainder = 11 % 3;

console.log (remainder)*/

//compound Assignment with Augmented Addition - 

/*var a = 3;
var b = 17;
var c = 12;

//Only modify code below this line

a = a + 12;
b = 9 + b;
c = c + 7;

shortcut to doing the function above is

a += 12;
b += 9;
c += 7;
*/

//compound Assignment with Augmented Subtraction -


/*var a = 11;
var b = 9;
var c = 3;

//Only modify code below this line

a = a - 6;
b = b - 15;
c = c - 1;

shortcut to doing the function above is

a -= 6;
b -= 15;
c -= 1;
*/


//compound Assignment with Augmented Multiplication -


/*var a = 11;
var b = 9;
var c = 3;

//Only modify code below this line

a = a * 5;
b = 3 * b;
c = c * 10;

shortcut to doing the function above is

a *= 5;
b *= 3;
c *= 10;
*/


//compound Assignment with Augmented Divison -


/*var a = 48;
var b = 108;
var c = 33;

//Only modify code below this line

a = a / 12;
b = b / 4;
c = c / 11;

shortcut to doing the function above is

a /= 12;
b /= 4;
c /= 11;
*/

//Declare String Variables 

//var firstName = "Allan";
//var lastName = "Turing";

/* string - characters surrounded by quotation marks - single, double or backticks (string Literals)

var myFirstName = "Belle"
var myLastName = "De Castro"*/

//Escaping Literal Quotes in String

/*var myStr = "I am a \"double quoted\"string inside\"double quotes\"";
console.log(myStr)

 Normally the quotes identify the beginning and the ending of the string

JS do not know what to do with the next following string - it will only read the first quoted words - escape character*

i will not show the \ - \ + quotation mark means it is just a plain quotation mark/



/* quoting strings with single quotes

- using a single quote - you do not need to use escape character \ for javascript to know that it is a single string

var myStr ='<a href =https://www.example.com" target="_blank">Link</a>';
console.log (myStr) */

/*****

- you can escape out the following characters 

CODE OUTPUT
\'		single quote
\" 		double quote
\\		backslash
\n 		newline
\r 		carriage return
\t 		tab
\b 		backspace
\f  	form feed

****

var myStr = "FirstLine\n\t\\SecondLine\nThirdLine"
console.log(myStr)*/

/*Concatenating strings with Plus Operator

var ourStr = "I come first."+"I come second";

var myStr = "This is the start. " + " This is the end."
console.log(myStr);

var ourStr = "I come first. ";

ourStr += "I come second.";

console.log (ourStr)

var myStr = "This is the first sentence. "
myStr += "This is the second sentence."
console.log(myStr)*/

/* Constructing Strings with variables */






