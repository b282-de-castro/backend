//console.log("Hello Belle!");

//[SECTION] Objects
// An object is a data type that is used to represent real world objects
// Information stored in objects are represented in a "Key: Value" pair

//Creating objects using object initializer/literal notation
/*
SYNTAX:

	let objectName = {
		keyA: valueA,
		keyB: valueB,
		...
	}

	*/

 let cellphone = {
 	name: 'nokia 3210',
 	manufactureDate: 1999
 };

console.log("Result from creating objects using initializer/literal notation: ")
console.log(cellphone);
console.log(typeof cellphone);

// creating objects using a constructor function

/* 
SYNTAX:
	function ObjectName(keyA, keyB) {
		this.keyA = keyA;
		this.keyB = keyB;
	}

*/
function Laptop(name, manufactureDate) {
		// this keyword allows to assign a new object's properties by asssociating them with values received from constructor function's parameter
		this.name = name;
		this.manufactureDate = manufactureDate;
	}

// "new" operator creates an instance of an object
let laptop = new Laptop ("Lenovo", 2008);
console.log(laptop);

let myLaptop = new Laptop ("Macbook Air", 2020);
console.log(myLaptop);

// [SECTION] Accessing object properties
// Using dot notation
console.log(myLaptop.name);
// Using square bracket notation
console.log(myLaptop['manufactureDate']);

//Accessing Array objects
let array = [laptop, myLaptop];
// square braket notation
console.log(array[0]['name']);
//dot notation
console.log(array[0].manufactureDate);

//[SECTION] Initializing/Adding, Deleting, Reassigning Object properties

let car = {};

//Initializing/adding object properties using dot notation

car.name = "Honda Civic";
console.log(car);

//Initializing/adding object properties using dot notation
car['manufactureDate'] = 2023;
console.log(car);

// deleting object properties
delete car['manufactureDate']; // or delete car.manufactureDate;
console.log(car);

// Reassigning object properties

car.name = "Dodge Charger R/T";
console.log(car);

//[SECTION] Object Methods
// A method is a function which is a property of an objects

let person = {
	name: "John",
	talk: function(){
		console.log("Hello, my name is " + this.name)
	}
}
console.log(person);
person.talk();

// Real World Application of Objects
//  Using object literals

let myPokemon = {
	name: "Pikachu",
	level: 3,
	health: 100,
	attack: 50,
	tackle: function(){
		console.log("This pokemon tackled targetPokemon.")
	},
	faint: function(){
		console.log("This pokemon fainted.")
	}
}
console.log(myPokemon);
myPokemon.tackle();
myPokemon.faint();

// Using constructor function

function Pokemon(name, level){
	//properties
	this.name = name;
	this.level = level; // 10
	this.health = 2 * level; //20
	this.attack = level;

	//method
	this.tackle = function(target){
		console.log(this.name + ' tackled ' + target.name);
		console.log("targetPokemon's health is now reduced to " + Number(target.health - this.attack));
	};
	this.faint = function() {
		console.log(this.name + ' fainted.');
	};
}

let pikachu = new Pokemon('Pikachu' , 16);
console.log(pikachu);

let rattata = new Pokemon ('Rattata' , 8);
console.log(rattata);

pikachu.tackle(rattata);
rattata.faint();