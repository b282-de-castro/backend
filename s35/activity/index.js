const express = require ("express");

//"mongoose" is a package that allows creation of schemas to model our data structures
// Also has access to a number of methods for manipulating our database
const mongoose = require ("mongoose");

const app = express();

const port = 3001;

//connecting to MongoDB Atlas
mongoose.connect("mongodb+srv://dcjobellene:pbhdDM4x9xdRibAg@wdc028-course-booking.drcmsgh.mongodb.net/s35",
	//allows us to avoid any current and future errors while connecting to MongoDB
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

// Connecting to MongoDB locally

let db = mongoose.connection;

db.on("error", console.error.bind(console,"connection error"));
db.once("open", () => console.log ("We're connected to the cloud database!"));

// Middleware

app.use(express.json());
app.use(express.urlencoded({extended: true}));

//[SECTION] Mongoose Schema
//Schemas determin teh structure of the documetns to be written in the database
// Schemas act as blueprints to our data

// The "new" keyword  create a new Schema
//USe the Schema() constructore of the Mongeese module 
/*const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		default: "pending"
	}

});
*/

const userSchema = new mongoose.Schema({
	name: String,
	password: String
});


const User = mongoose.model("User", userSchema);

app.post('/signup', (req, res) => {

	User.findOne({ name: req.body.name }).then((result, err) => {

		if (result != null && result.name === req.body.name) {
			return res.send("Duplicate username found!");

		} else {

			if (req.body.name != null && req.body.password != null) {
				let newUser = new User({
					name : req.body.name,
					password : req.body.password
				});

				newUser.save().then((savedUser, saveErr) => {

					if (saveErr) {
						return console.error(saveErr)
					} else {
						return res.status(201).send("New User Registered!")
					}
				})
			} else {
				return res.send('BOTH username and password must be provided.');
			}
		}
	})
})

//[SECTION] Models
/*
const Task = mongoose.model("Task", taskSchema);

*/
/*
Creating a new task
1. Add a functionality to check if there are duplicate tasks
	- If the task already exists in the database, we return a message
	- If the task doesn't exist in the database, we add it in the database
2. The task data will be coming from the request's body
3. Create a new Task object with a "name" field/property
4. The "status" property does not need to be provided because our schema defaults it to "pending" upon creation of an object
*/
/*
app.post("/tasks", (req, res) => {
	Task.findOne({name: req.body.name}).then((result, err) => {
		if (result != null && result.name == req.body.name) { return res.send("Duplicate task found!");
		} else {
			let newTask = new Task({
				name: req.body.name
			});
			newTask.save().then ((savedTask, saveErr) => {
				if(saveErr){
					return console.error(saveErr);
				} else {
					return res.status(201).send("New Task Created!");
				}
			})
		}
		
	})

})

*/


/*
Getting all the tasks
1. Retrieve all the documents
2. If an error is encountered, print the error
3. If no errors are found, send a success status back to the client/Postman and return an array of documents
*/
/*
app.get("/tasks", (req, res) => {
	Task.find({}).then((result, err) => {
		if (err) {
			return console.log (err);
		} else {
			return res.status(200).json({
				data: result
			})
		}
	})
})
*/




app.listen(port, () => console.log(`Server running at port ${port}!`));
