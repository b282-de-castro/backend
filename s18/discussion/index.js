//console.log("Hello world!")

// Functions
/*function printInput(){
	let nickname = prompt("Enter your nickname:");
	console.log("Hi, " + nickname);

}
printInput();*/

// Parameters and Arguments

//(name) - is called a parameter
// A "parameter" acts as a named variable/contaier that exists only inside a function
function printName(name){
	console.log ("My name is " + name);
}

// ("Juana") - the information/data provide directly into the function is called an argument
printName("Juana");
printName("John");
printName("Jane");

// Variables can also be passed as an argument
let sampleVariable = "Yui";
printName(sampleVariable);

function checkdivisibilityBy8(num){
	let remainder = num % 8;
	console.log("The remainder of " + num + " divided by 8 is: " + remainder);
	let isDivisibleBy8 = remainder ===0;
	console.log ("Is " + num + " divisible by 8?")
	console.log(isDivisibleBy8);
}
checkdivisibilityBy8(64);
checkdivisibilityBy8(65);
checkdivisibilityBy8(28);

//Functions as Arguments
function argumentFunction () {
	console.log("This function was passed as an argument before the message was printed.")
}

function invokeFunction(argument){
	argumentFunction();
}

invokeFunction(argumentFunction);
console.log(argumentFunction);

//Multiple "arguments" will corrspond to the number of "parameters" declared in a function in succeeding order.

function createFullName(firstName, middleName, lastName) {
	console.log ( firstName + ' ' + middleName + ' ' + lastName);
}
// "Juna" will be stored in the parameter "firstname"
// "Dela" will be stored in the parameter "middleName"
// "Cruz" will be stored in the parameter "lastName"
createFullName ('Juan' , 'Dela' , 'Cruz');
createFullName ('Juan' , 'Dela' , 'Cruz' , "Hello");
createFullName ('Juan' , 'Dela');

// Using variable as arguments
let firstName = "John";
let middleName = "Doe";
let lastName = "Smith";

createFullName(firstName, middleName, lastName);

function createfullname(middleName, firstName, lastName){
	console.log ( firstName + ' ' + middleName + ' ' + lastName);
}

createFullName('Juan' , 'Dela' , 'Cruz');