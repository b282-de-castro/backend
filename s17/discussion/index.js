//console.log("Hello World!");

// [SECTION] Functions
//Functions in JS are lines/block of codes that tell our device/application to perfimr a certain task when called/invoked

// Function declaration
/*
SYNTAX:
	function functionName (){
		code block (statement)
	}
*/
 
// function - keyword used to define a JS functions
// printName - function name. 
// Functions are named to be able to be used later in the code
function printName() {
	// function block {} - the statements which comprise the body of the function
	console.log("My name is Belle.");
}

// Function Invocation
printName();

// [SECTION] Function Declaration vs expressions
//Function Declaration
// A function can be created through function declaration by using FUNCTION keyword and adding a function name

declaredFunction(); //pwede siya for hoisting

function declaredFunction() {
	console.log("Hello World from declaredFunction()!");
}
declaredFunction();


// Function Expressions
// A function can also be storesd in a variable
// A dunction expression is an anonymous function assigned to a variableFunction
//Anonymous function  - function without a name

//variableFunction(); //result : Uncaught ReferenceError: Cannot access 'variableFunction' before initialization

let variableFunction = function() {
	console.log("Hello again!");
}
variableFunction();

// Function Expressions are always invoked/called using variable name
let funcExpression = function funcName() {
	console.log("Hello from the other side!")
}

//funcName();//result: funcName is not defined

funcExpression();

// You can re assign function declaraitons and function expressions to new anonymous functions


declaredFunction = function() {
	console.log("Updated declaredFunction!")
}

declaredFunction();

funcExpression = function() {
	console.log("Updated funcExpression!")
}
funcExpression();

const constantFunc = function() {
	console.log("Initialized with const!")
}
constantFunc();

/* constantFunc = function() {
	console.log	("Cannot be reassigned!")
} 
constantFunc();
result:  Assignment to constant variable. */

//[SECTION] Function scoping
//Scope is the accessibility/visibility of variables
/*
JS has 3 types of scope
1. local/block scope
2. global scope
3. function scope
*/

{
	let localVar = "Armado Perez";
	console.log(localVar);
}


let globalVar = "Mr. WorldWide";
console.log(globalVar);
//console.log(localVar);// localVar is not defined

function showNames() {

	//function scope variables
	var functionVar = "Joe";
	const functionConst = "John";
	let functionLet = "Jane";

	console.log	(functionVar)
	console.log	(functionConst)
	console.log	(functionLet)
}

showNames();

// console.log	(functionVar); will result to error
// console.log	(functionConst); will result to error
// console.log	(functionLet);will result to error

//Global Scoped Variable
let globalName = "Alexandro"

function myNewFunction2 () {
	let nameInside = "Renz";

	console.log(globalName)
}

myNewFunction2();


// Nested Functions
// YOu can create another function inside a function

function myNewFunction() {
	let name  = "B282";

	function nestedFunction(){
		let nestedName = "PT Class"
		console.log(name);
	}
	nestedFunction();
}
myNewFunction();

//[SECTION] Using Alert ()
// alert() allows us to show a small window at the top of our browser page to show information to our users

alert ("Hello World!"); // This is will run immediately when the page loads

function showSampleAlert(){
	alert("Hello, User!")
}
showSampleAlert();

let samplePrompt = prompt ("Enter your name");
console.log ("Hello, " + samplePrompt)

function printWelcomeMessage() {
	let firstName = prompt ("Enter your first name.");
	let lastName = prompt("Enter your last name.")

	console.log (" Hello, " + firstName + " " +lastName + "!")
	console.log ("Welcome to my Page!")
}

printWelcomeMessage();