//console.log("Hello Belle!");


// [SECTION] JavaScript Synchronous and Asynchronous
//default JS is Synchronous ( 1 line a a time - line by line)

//conosole.log("Hello Belle!");
console.log("Hello Belle!");

//Error

///asynchronous means that we can proceed to execute other statemens, while consuming is running in the background

//[SECTION] Getting all the post
// The Fetch API allows you to asynchoronously request for a resource (data)
/*
SYNTAX: 
	fetch('URL')

*/

console.log(fetch('https://jsonplaceholder.typicode.com/posts/'));

// Fecth and .then

/*
SYNTAX: 
	fetch('URL')
	.then(response)=>{})
*/

fetch('https://jsonplaceholder.typicode.com/posts/')
.then(response => console.log(response.status));

fetch('https://jsonplaceholder.typicode.com/posts/')
.then(response => console.log(response));


fetch('https://jsonplaceholder.typicode.com/posts/')
.then(response => response.json())
.then((json) => console.log(json));



// Async and Await

async function	fetchData(){
	let result = await fetch('https://jsonplaceholder.typicode.com/posts/');
	console.log(result);
	console.log(typeof result);
	console.log(result.body);

	let json = await result.json()
	console.log(json);
}

fetchData();



//[SECTION] Creating a POST

fetch('https://jsonplaceholder.typicode.com/posts/', {
	method: 'POST', 
	headers: {
		'content-Type':'application/json'
	},
	body: JSON.stringify({
		userId: 1,
		title: "Create",
		body: "Body of Post"

	})
})

.then(response => response.json())
.then((json) => console.log(json));


//[SECTION] UPDATE a post

fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'PUT', 
	headers: {
		'content-Type':'application/json'
	},
	body: JSON.stringify({
		userId: 1,
		title: "Update",
		body: "Body of Post"

	})
})

.then(response => response.json())
.then((json) => console.log(json));

//[SECTION] DELETE a POST

fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'DELETE', 
});

