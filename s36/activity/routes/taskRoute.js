//Defines WHEN particular controllers will be used
// COntains all the endpoints for our application

const express = require ("express");

//creates a router instance that functions as a middleware and routing system
const router = express.Router();
// The "taskController" allows us to use the functions defined in the "taskController.js" file
const taskController = require ("../controllers/taskController");

//[SECTION] Routes
//http://localhost:3001/tasks/
router.get ("/", (req, res) => {
	taskController.getAllTasks().then(resultFromController => res.send(resultFromController));
});

// Route to create a new task
router.post("/", (req, res) => {
	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
});


// Delete task
/*
Business Logic
1. Look for the task with the corresponding id provided in the URL/route
2. Delete the task using the Mongoose method "findByIdAndRemove" with the same id provided in the route
*/

router.delete("/:id", (req, res) => {
	taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController));
});


//Updating a task

router.put("/:id", (req, res) => {
	taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
});

//get specific task

router.get ("/:id", (req, res) => {
	taskController.getSpecificTask().then(resultFromController => res.send(resultFromController));
});

//complete a specific task
router.put("/:id/complete", (req, res) => {
	taskController.completeSpecificTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
});
















module.exports = router;

