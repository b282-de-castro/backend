//console.log("hello Belle")

//[SECTION] Exponent Operator

console.log("Exponent Operator:");
//"Math.pow" method takes two arguments, the base and the exponent
// raise 8 to the power of 2
const firstNum =Math.pow(8, 2); //64
console.log(firstNum);

// **operator is used for the exponentiation
// raise 8 to the power of 2
const secondNum = 8 ** 2;
console.log(secondNum);

// [SECTION] Template Literals
// Allows to write strings without using concatenation operator "+"

let name = "John";

let message = 'Hello ' + name + '! Welcome to programming!';
console.log(message);

//uses "``"
message = `Hello ${name}! Welcome to programming!`;
console.log(message);

const interest = .1;
const principal = 1000;

console.log(`The interest on your savings account is ${principal * interest}.`);


//[SECTION] Array Destructuring
// allows to unpack elements in arrays into distinct variables

/*
SYNTAX;
	let/const [variableName, variableName, variableName ...] = arrayName;

*/

const fullName = ['Juan', 'Dela', 'Cruz'];
console.log(fullName[1]);

const [firstName, middleName, lastName] = fullName;
console.log(middleName);

//[SECTION] Object Destructuring
// allows to unpack elements in object into distinct variables

/*
SYNTAX;
	let/const{propertyName, propertyName, propertyName ...} = objectName;

*/

const person = {
	givenName: "Jane",
	maidenName: "Dela",
	familyName: "Cruz"
}

console.log(person.familyName);

const { givenName, maidenName, familyName} = person
console.log(familyName);


//[SECTION] Arrow Functions
// arrow functions allow us to write shorter function syntax.

/*
SYNTAX:
	let/const variableName=() => {
	 	statement
	}

*/

const students = ["John" , "Jane" , "Judy"];

students.forEach(function(student){
	console.log(`${student} is a student.`);
});

// Arrow Function
students.forEach((student)=>{
	console.log (`${student} is a student.`);
});

//[SECTION] Implicit Return Statemen
//Tehre are instances when you can omit the "return" statement

/*const add = function(x, y) {
	return x + y;
}

const total = add (1, 2);
console.log(total); */

// Arrow Function
console.log ("Result from usingArrow function:");

const add = (x, y) => x + y;
const total = add (1, 2);
console.log(total);

// [SECTION] Default Function Argument Value

/*const greet = (name = "User") => {
	return `Good morning, ${name}!`;
}*/

const greet = (name = "User") => 
`Good morning, ${name}!`;


console.log(greet("B282"));
console.log(greet());


//[SECTION] Class-Based Objects Bleprints
//Allows creation/instatiation of Bojects using classes as blueprints

class Car {
	constructor(brand, name, year) {
		this.brand = brand;
		this.name = name;
		this.year = year;
	}
}

const myCar = new Car (); 
console.log(myCar);

//creating/ Instantiating  a new object from car class with initialized values

const myNewCar =new Car("Toyota", "Vios", "2021")
console.log(myNewCar);

// Values of properties may be assigned after creation/instatiation of Objects

myCar.brand = "Ford";
myCar.name = "Ranger Raptor";
myCar.year = 2021;
console.log(myCar);