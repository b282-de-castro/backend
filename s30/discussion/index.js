// Create documents to use for the discussion
db.fruits.insertMany([
  {
    name : "Apple",
    color : "Red",
    stock : 20,
    price: 40,
    supplier_id : 1,
    onSale : true,
    origin: [ "Philippines", "US" ]
  },

  {
    name : "Banana",
    color : "Yellow",
    stock : 15,
    price: 20,
    supplier_id : 2,
    onSale : true,
    origin: [ "Philippines", "Ecuador" ]
  },

  {
    name : "Kiwi",
    color : "Green",
    stock : 25,
    price: 50,
    supplier_id : 1,
    onSale : true,
    origin: [ "US", "China" ]
  },

  {
    name : "Mango",
    color : "Yellow",
    stock : 10,
    price: 120,
    supplier_id : 2,
    onSale : false,
    origin: [ "Philippines", "India" ]
  }
]);


//Using the aggregate method

/*
  SYNTAX:  
  db.collectionName.aggregate([
    {$match: {field
    : valueA}},
    {$group: {_id: "SfieldB"}}, {result:{operation}}
  ])
*/

db.fruits.aggregate([
    {$match: {onSale: true}},
    {$group: {_id: "$supplier_id", total: {$sum:"$stock"}}}
  ]);


//Field projection with aggregation

/*
 $project : {field: 1/0}

*/

db.fruits.aggregate([
    {$match: {onSale: true}},
    {$group: {_id: "$supplier_id", total: {$sum:"$stock"}}},
    {$project: {_id: 0}}
  ]);


//Sorting aggregated result

/*
SYNTAX:
{SORT : field : 1/-1 }

*/

db.fruits.aggregate([
    {$match: {onSale: true}},
    {$group: {_id: "$supplier_id", total: {$sum:"$stock"}}},
    {$sort: {total: 1}}
  ]);


//Aggregating results, based on array field
/*
SYNTAX:
  {$unwind:field}
*/
// $unwind deconstructs an array field from a collection with an array value to output a result for each element
db.fruits.aggregate([
    {$unwind:"$origin"}
  ]);

// Displays  fruit documents bytheir origin and the kinds of furits that are supplied
db.fruits.aggregate([
    {$unwind:"$origin"},
    {$group: {_id: "$origin", kind_of_fruits: {$sum:1}}},
  ]);
