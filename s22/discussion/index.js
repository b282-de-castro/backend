//console.log("Hi Belle!")

//Array Methods
//JS has built-in functions and Methods for arrays
//This allows us to manipulte and Access array items

//[SECTION] Mutator Methods
//Mutator methods are functions that mutate or change an array after they are created

let fruits = ['Apple' , 'Orange' , 'Kiwi' , 'Dragon Fruit']; 
console.log("Original Array:");
console.log(fruits); //['Apple', 'Orange', 'Kiwi', 'Dragon Fruit']

// push()
//adds an element in the end of an array and returns the updated array's length
/*
SYNTAX:
	arrayName.push();
*/ 

let fruitsLength = fruits.push('Mango');
console.log	("fruitsLength:");
console.log	(fruitsLength);//5
console.log	("Mutated array from push() method:");
console.log	(fruits); //['Apple', 'Orange', 'Kiwi', 'Dragon Fruit', 'Mango']

// Pushing multiple elements to an array
fruitsLength = fruits.push('Avocado' , 'Guava');
console.log	("fruitsLength:");
console.log	(fruitsLength);//7
console.log	("Mutated array from push() method:");
console.log	(fruits); //['Apple', 'Orange', 'Kiwi', 'Dragon Fruit', 'Mango', 'Avocado', 'Guava']

// pop()
//Removes the last element and return the removed element
/*
SYNTAX:
	arrayName.pop();
*/

console.log("Current Array;");
console.log(fruits);

let removedFruit = fruits.pop();
console.log("Removed Fruit:");
console.log(removedFruit);
console.log(fruits);


// unshift()
//it adds one or more elements at the beginning of an array and it returns the updated array's length

/*
SYNTAX:
arrayName.unshift('ElementA');
arrayName.unshift('ElementA, ElementB ...');

*/
console.log("Current Array;");
console.log(fruits);
fruitsLength = fruits.unshift('Lime' , 'Banana');

console.log(fruitsLength);
console.log("Mutated array from unshift() method:");
console.log(fruits);


// shift()
// removes an element at the beginning of an array and returns the removed element

/*
SYNTAX:
arrayName.shift();
*/

console.log("Current Array;");
console.log(fruits);
removedFruit = fruits.shift();

console.log('removedFruit:')
console.log(removedFruit);
console.log("Mutated array from shift() method:");
console.log(fruits);

// splice()
// simultaneously removes an element from a specified index number and adds element

/*
SYNTAX:
	arrayName.splice(startingIndex, deleteCount, elementsToBeAdded);
*/

console.log("Current Array;");
console.log(fruits);

fruits.splice(1, 2, 'Lime' , 'Cherry');
console.log("Mutated array from splice() method:");
console.log(fruits);

// sort()
//rearranges the array element in alphanumeric order

/*
SYNTAX:
arrayName.sort();
*/

console.log("Current Array;");
console.log(fruits);

fruits.sort();
console.log("Mutated array from sort() method:");
console.log(fruits);

/*let number = [54, 86, 28, 42, 99, 72];
console.log(number);
number.sort();
console.log(number);*/

// reverse ()
//reverses the order of aray elements

/*
SYNTAX:
arrayName.reverse();
*/

console.log("Current Array;");
console.log(fruits);

fruits.reverse();
console.log("Mutated array from reverse() method:");
console.log(fruits);

//[SECTION] Non-mutator method
// Non -mutator methods are functons that do not modify or change an array after they are created

let countries =['US' , 'PH' , 'CA' , 'SG' , 'TH' , 'PH' , 'FR' , 'DE' ];

console.log(countries); 

//indexOf()
// returns the index number of the first matching element found in an array

/*
SYNTAX:
arrayName.indexOf(searchValue);
arrayName.indexOf(searchValue, startingIndex);
*/

let firstIndex = countries.indexOf('PH');
console.log("Result of firstIndex:");
console.log(firstIndex);
firstIndex = countries.indexOf('PH' , 2);
console.log(firstIndex);

// If no match was fond, the result will be -1

firstIndex = countries.indexOf('BR' , 2);
console.log(firstIndex);

// lastIndexOf()
// returns the indx number of the last maching element found in an array

let lastIndex = countries.lastIndexOf('PH');
console.log("Result of lastIndex:");
console.log(lastIndex);

// slice
// portions/slices elements from an array and return a new array

/*
 arrayName.slice(startIndex);
 arrayName.slice(startIndex, endingIndex);

 */

let slicedArrayA = countries.slice(2);
console.log("Result from slice() method:");
console.log(slicedArrayA);

//The elements that will be sliced are elements from the starting index until the el;ement BEFORE the ending index
let slicedArrayB = countries.slice(1 , 5);
console.log(slicedArrayB);

// toString()
// return an array as string separated by commas

/*
 arrayName.toString();

 */

let stringArray = countries.toString();
console.log('Result from toString() method');
console.log(stringArray);
console.log(typeof stringArray);

//[SECTION] Iteration Methods
//loops designed to perform repetitive task
//loops over all items in an array

//forEach()
//similar toa  for loop that iterates on each array of elements

/*
SYNTAX:
	arrayName.forEach(function(indivElement) {
		statement
	})

	*/

countries.forEach(function(country) {
		console.log(country)
	});

//map()
// iterates on eahc element and returns new array with diffrent valus depending on the result of the function's operation

/*

SYNTAX:
 let/const resultArray = arrayName.map(function(invidElement){
	 statement
 })
	
 */

let numbers = [1, 2, 3, 4, 5]

let numberMap = numbers.map(function(number){
	return number * number
});

console.log("Original Array:")
console.log(numbers)
console.log("Result of map() method:")
console.log(numberMap)

//filter ()
// returns new array that contains elements which meets the given condition

/*

SYNTAX:
 let/const resultArray = arrayName.filter(function(invidElement){
	 statement
 })
	
 */

 let filterValid = numbers.filter(function(number){
	 return (number < 3);
 })

 console.log("Result of filter() method:")
console.log(filterValid)

// includes()
// checks if the argument passed can be found in the array
//answers a boolean

/*
arrayName.includes(<argumentToFind>)
*/

let product = ["Mouse" , "Keyboard" , "Laptop" , "Monitor"];

let productFound1 = product.includes("Maouse");
console.log("Result from includes() method:");
console.log(productFound1);



