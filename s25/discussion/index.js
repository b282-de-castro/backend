//console.log('Hello B282');
/*

JSON
JAVASCRIPT OBJECT NOTATION
-Also used in other programming languages
-Core JavaScript has a built in JSON object that contains methods for parsing JSON objects and converting strings into JavaScript Objects

SYNTAX:
{
	"propertyA":"valueA"
	"propertyB":"valueB"
};

JSON are wrapped in curly braces
Properties and values are wrapped in double quotes

*/

let sample1 = `
	{
		"name":"Mochi",
		"age":20,
		"address":{
			"city":"Tokyo",
			"country":"Japan"
		}
	}
`;

console.log(sample1);

//create a variable that will hold a "JSON" and create a person object
//log the variable an send ss

let person = `
	{
		"name":"Macky",
		"age":14,
		"address":{
			"city":"Davao",
			"country":"Philippines"
		}
	}
`;

console.log(person);
console.log(typeof person);//string


//Are we able to turn a JSON into a JS Object?
//JSON.parse() - will return the JSON as an object

console.log(JSON.parse(sample1));
console.log(JSON.parse(person));

//JSON Array
//is an array of JSON

let sampleArr = `
[
	{
		"email": "mochi@gmail.com",
		"password": "mochimochi",
		"isAdmin": false
	},
	{
		"email": "zenitsu@proton.me",
		"password": "agatsuma",
		"isAdmin": true
	},
	{
		"email": "json@gmail.me",
		"password": "friday13",
		"isAdmin": false
	}
]
`;

console.log(sampleArr);
console.log(typeof sampleArr);


//can we use array methods on a JSON Array? (sampleArr)
//No. Because a JSON is a string

// So what can we do to be able to add mnore items/objects into our sampleArr?
//PARSE the JSON array for us to use Array Methods and save it in the variable

let parsedSampleArr = JSON.parse(sampleArr);
console.log(parsedSampleArr);
console.log(typeof parsedSampleArr);

console.log(parsedSampleArr.pop());
console.log(parsedSampleArr);



// if for example, we nee to send this back toour client/Front End, it should be in JSON Format

//JSON.Stringify - this will stringify JS objects as

sampleArr = JSON.stringify(parsedSampleArr);
console.log(sampleArr);

//Database (JSON) => sercer/API (JSON to JS Object to process) => sent as json (frontend/client)

let jsonArr =`[
		"pizza",
		"hamburger",
		"spaghetti",
		"shanghai",
		"hotdog stick on a pineapple",
		"pancit bihon"
	]`;
//convert json string to js array
let parseJsonArr = JSON.parse(jsonArr);

//delete the last item in the array
parseJsonArr.pop();

//add a new item to the array
parseJsonArr.push("cake");

//update the JSONarr variable with the stringified array
jsonArr = JSON.stringify(parseJsonArr);
//log the updated jsonArr
console.log(jsonArr);

//Gather User Details

let firstName = prompt("What is your first name?");
let lastName = prompt("What is your last name?");
let age = prompt("What is age?");
let address = {
	city: prompt("From which city do you live in?"),
	country: prompt("From which counry does your city address belong to?")
};

let otherData = JSON.stringify({

	firstName: firstName,
	lastName: lastName,
	age: age,
	address: address

})

console.log(otherData);



let sample3 = `
{
	"name": "Cardo",
	"age": 18,
	"address":{
		"city":"Quiapo",
		"country":"Philippines"
	}
}`;

try {
	console.log(JSON.parse(sample3))
}
catch(err){
	console.log("Error Message:")
	console.log(err)
}finally {
	console.log("This will run!")
}



